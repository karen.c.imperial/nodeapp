#!/bin/bash 
#$CHECK HOLDS THE STATUS FOR THE JOB THAT RUN IN THE SCRIPT STAGE
echo "checking for the exit status of previous stage"
CHECK="$(cat /tmp/${CI_JOB_ID})"
echo $CHECK
curl  -X POST -H "Content-type: application/json" \
-d '{

 "title": "A deployment has been triggered for the following project: '"${CI_PROJECT_TITLE}"'",
 "text": "Commit description: '"${CI_COMMIT_TITLE}"' | Commit SHA: '"${CI_COMMIT_SHA}"' | Job Name: '"${CI_JOB_NAME}"' | Job ID: '"${CI_JOB_ID}"' | Job Stage: '"${CI_JOB_STAGE}"'",
 "tags": 
 

 [
 "STATUS:'"$CHECK"'", 
 "GITLAB_USER:'"$GITLAB_USER_LOGIN"'",
 "APIGEE_ENVIRONMENT:'"$APIGEE_ENVIRONMENT"'",
 "ENVIRONMENT:'"$CI_ENVIRONMENT_NAME"'",
 "PROJECT_NAME:'"$CI_PROJECT_NAME"'",
 "PROJECT_ID:'"$CI_PROJECT_ID"'"
 ]


 }' \
"https://api.datadoghq.eu/api/v1/events?api_key=$API_KEY"














